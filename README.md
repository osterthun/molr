# molr


molr (pronounced molar) is a command-line tool to generate input files for
quantum chemical calculations and retrieval of general results such as energies
and thermodynamic corrections from completed calculations. It is currently tailored
towards ORCA and the Slurm submission system at RWTH Aachen University. 

This work is inspired by the [tools-for-g16.bash](https://github.com/polyluxus/tools-for-g16.bash)
and [tools-for-orca.bash](https://github.com/polyluxus/tools-for-orca.bash).

## Table of Contents
- [Installation](#installation)
- [Configuration](#createconfig)
- [CLI Usage](#cliusage)
    - [prepare](#prepareusage)
        - [Example](#prepareexample)
    - [submit](#submitusage)
        - [Example](#submitexample)
    - [freqinput](#freqinputusage)
        - [Example](#freqinputexample)
    - [getenergy](#getenergyusage)
    - [getfreq](#getfrequsage)

## Installation {#installation}

1. Clone this git repository `git clone https://github.com/O2-AC/molr.git`.
2. Change into the directory `cd molr`.
3. Create a virtual environment `python -m venv venv`.
4. Activate enviroment `. venv/bin/activate`.
5. Install package `pip install -e .`
6. Get path of current directory `pwd`.
7. Create a link to a folder in your `PATH`, e.g. `~/bin`: `ln -s $(pwd)/venv/bin/molr ~/bin/molr`.
8. Deactive virtual environment `deactivate`.
9. You now should have access to the command `molr`, try it out with `molr -h`.
10. Finally [create the config file](#createconfig)

## Create the config file {#createconfig}
The repository already provides a preliminary config file `.molrrc`. I suggest
you copy this file to your home directory. From inside the `molr` directory run:
```bash
$ cp .molrrc $HOME/.molrrc
```

You can now open the copy in your home folder and edit it accordingly. That file
will take precedence when `molr` is trying to read the configuration.

Note: Although you find already options for local ORCA installs, these are
currently not implemented for the `submit` command! To run locally you manually
have to make sure ORCA and OpenMPI are installed on your machine and then run:
```bash
$ /full/path/to/orca water.inp > water.out
```
All other commands of `molr` are not affected and can be run locally in any case.

## CLI Usage {#cliusage}

Initial help can be found with `molr -h` and `molr <subcommand> -h`.

```bash
$ molr -h
usage: molr [-h] {prepare,freqinput,submit,getfreq,getenergy,version} ...

positional arguments:
  {prepare,freqinput,submit,getfreq,getenergy,version}
    prepare             Prepares an input file for ORCA run
    freqinput           Prepares an input file for FREQ run
    submit              Prepares and submits Slurm job
    getfreq             Returns therm. chem. results of FREQ run
    getenergy           Returns final single point energy
    version             Displays version of molr

options:
  -h, --help            show this help message and exit
```

### prepare command {#prepareusage}

```
usage: molr prepare [-h] [-f FUNCTIONAL] [-b BASIS_SET] [-c CHARGE]
                    [-m MULTIPLICITY] [-u USER] [-O OUTPUT] [-o] [-d]
                    [--debug] [--quiet]
                    coord_file

positional arguments:
  coord_file

options:
  -h, --help            show this help message and exit
  -f FUNCTIONAL, --functional FUNCTIONAL
                        DFT functional
  -b BASIS_SET, --basis-set BASIS_SET
                        Basis set
  -c CHARGE, --charge CHARGE
                        Molecular charge
  -m MULTIPLICITY, --multiplicity MULTIPLICITY
                        Multiplicity
  -u USER, --user USER  Custom options to add to the input file
  -O OUTPUT, --output OUTPUT
                        Output file name
  -o, --opt             Include structure optimization
  -d, --dispersion      Include dispersion correction (WIP, might not work for
                        all functionals!)
  --debug               Outputs debug information
  --quiet               Only outputs errors
```

#### Example: Create an input file for a structure optimization {#prepareexample}
For this example the structure of water is optimized with the BP86 functional on 
the def2-SVP basis set. The initial guess of the water structure is present as
a `water.xyz` file.

```bash
$ molr prepare -f BP86 -b DEF2-SVP -c 0 -o water.xyz
```

This will create a `water.inp` file with the following content:
```
! RKS BP86 DEF-SVP   OPT
%PAL NPROCS 24 END
* xyz 0 1
<coordinates from your water.xyz file>
*
```

Note: `molr` automatically added the number of processors/threads to use from the
`.molrrc` file (see Configuration).

If you want to optimize to tighter convergence criteria, you can use the `-o` option
multiple times. 

| option | keyword |
| ------ | ------- |
| -o      | OPT     |
| -oo     | TightOPT |
| -ooo    | VerytightOpt |

For example, to optimize to the `TightOPT` level:
```bash
$ molr prepare -f BP86 -b DEF2-SVP -c 0 -oo water.xyz
```

If you would want to enable dispersion corrections you can use the `-d` flag:
For example, to optimize to the `TightOPT` level:
```bash
$ molr prepare -f BP86 -b DEF2-SVP -c 0 -oo -d water.xyz
```

Resulting in:
```
! RKS BP86 DEF2-SVP  D3 TightOPT 
%PAL NPROCS 24 END 
* xyz 0 1
<coordinates from your water.xyz file>
*
```

### submit command {#submitusage}
To run the previously generated input file with ORCA, `molr` is currently hardcoded
to write a submission script for Slurm as configured at RWTH's `claix18` HPC. 

```
usage: molr submit [-h] [-c CPUS] [-m MEM_PER_CPU] [-w WALLTIME] [-j JOB_NAME]
                   [-d DEPENDS_ON] [-a ACCOUNT] [--dry-run]
                   input_file

positional arguments:
  input_file            ORCA input file to be run

options:
  -h, --help            show this help message and exit
  -c CPUS, --cpus CPUS  Number of CPUs per node for Slurm job
  -m MEM_PER_CPU, --mem-per-cpu MEM_PER_CPU
                        Amount of memory allocated per CPU
  -w WALLTIME, --walltime WALLTIME
                        Walltime of batch job
  -j JOB_NAME, --job-name JOB_NAME
                        Name of the Slurm job
  -d DEPENDS_ON, --depends-on DEPENDS_ON
                        Job id of depending calculation
  -a ACCOUNT, --account ACCOUNT
                        HPC project to bill core-h to
  --dry-run             Generate Slurm script but do not submit to the queue
```

#### Example: Submitting an optimization input file to Slurm {#submitexample}

To submit the above created input file for the structure optimization of water,
which has the filename `water.inp` you can run:

```bash
$ molr submit -c 24 water.inp
```

If you would like to inspect the file before and submit it manually you can use
the `--dry-run` option:

```bash
$ molr submit -c 24 --dry-run water.inp
```

This will create `water.slurm.bash` file, which can be submitted manually via:
```bash
$ sbatch water.slurm.bash
```

### freqinput command {#freqinputusage}

```
usage: molr freqinput [-h] [-T TEMPERATURE] [-P PRESSURE] input_file

positional arguments:
  input_file            Input file of a completed geometry optimization

options:
  -h, --help            show this help message and exit
  -T TEMPERATURE, --temperature TEMPERATURE
                        Temperature in Kelvin
  -P PRESSURE, --pressure PRESSURE
                        Pressure in atm
```

#### Example: Preparing a frequency calculation {#freqinputexample}

Assuming the structure optimization of water has been completed successfully, you can
create a separate frequency job as follows:

```bash
$ molr freqinput water.inp
```

This will search for an `.xyz` file with the same basename, here: `water.xyz`. Instead
of copying the coordinates to the input file, `freqinput` will reference the `.xyz`
file with the `xyzfile` directive. Therefore, in case the optimization is still running
you can mark the optimization Slurm job as a dependency when submitting. Once the
frequency calculation then starts, it can be assumed that `water.xyz` has been 
overwritten by ORCA with the optimized geometry.

The input file `water.freq.inp` from the command above will look like this:

```
! RKS BP86 DEF2-SVP D3 FREQ 
%PAL NPROCS 24 END 
%maxcore 1600
* xyzfile 0 1 test.xyz 
```

Note: The `%maxcore` directive is added automatically based on the memory per core
specified in the configuration file (see above). 

### getenergy command {#getenergyusage}

```
usage: molr getenergy [-h] [output_file]

positional arguments:
  output_file  Output file to extract energy from

options:
  -h, --help   show this help message and exit
```

### getfreq command {#getfrequsage}

```
usage: molr getfreq [-h] output_file

positional arguments:
  output_file  File of a completed frequency calculation

options:
  -h, --help   show this help message and exit
```


## Package Usage
TODO: Show examples on how to import molr and use functions in python scripts.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
