# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Entry point file for CLI"""
import logging
import argparse
from importlib import metadata
from .prepare import prepare_arguments, prepare_orca_input
from .getenergy import getenergy_arguments, get_energy
from .submit import (
    submit_arguments,
    create_submit_file,
    write_submit_file,
    submit_submit_file,
)
from .readconfig import read_config
from .freqinput import freqinput_arguments, create_freqinput_orca
from .getfreq import get_freq, getfreq_arguments

logging.basicConfig(level=logging.INFO, format="%(levelname)-8s : %(message)s")

config = read_config()


def getenergy(args):
    results = get_energy(output_file=args.output_file)
    print(f"{'Electronic energy:':<18} {results.electronic_energy} Eh")
    print(f"{'Functional:':<18} {results.functional}")
    print(f"{'Full path:':<18} {results.full_path}")


def getfreq(args):
    results = get_freq(output_file=args.output_file)
    print(f"{'Gibbs correction:':<17} {results.gibbs_correction} Eh")
    print(f"{'Temperature:':<17} {results.temperature} K")
    print(f"{'Pressure:':<17} {results.pressure} atm")
    print(f"{'Full path:':<17} {results.full_path}")


def getversion():
    version = metadata.version("molr")
    print(f"{version}")


def prepare(args):
    prepare_orca_input(args, config)


def submit(args):
    submit_file = create_submit_file(
        input_file=args.input_file,
        cpus=args.cpus,
        account=args.account,
        job_name=args.job_name,
        walltime=args.walltime,
        dependency=args.depends_on,
        mem_per_cpu=args.mem_per_cpu,
    )
    submit_path = args.input_file[:-4] + ".slurm.bash"
    write_submit_file(
        slurm_script_content=submit_file, slurm_script_path=submit_path
    )

    if args.dry_run:
        logging.info(
            "Slurm script generated successfully! Script path: %s",
            submit_path,
        )
    else:
        # TODO: Add proper check if sbatch exited without error and also parse the
        #       job id which is returned by sbatch
        submit_submit_file(slurm_file=submit_path, job_name=args.job_name)


def main():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="subcommand")

    prepare_parser = subparsers.add_parser(
        "prepare", help="Prepares an input file for ORCA run"
    )
    for arg in prepare_arguments:
        prepare_parser.add_argument(*arg[0], **arg[1])
    prepare_parser.set_defaults(func=prepare)

    freqinput_parser = subparsers.add_parser(
        "freqinput", help="Prepares an input file for FREQ run"
    )
    for arg in freqinput_arguments:
        freqinput_parser.add_argument(*arg[0], **arg[1])
    freqinput_parser.set_defaults(func=create_freqinput_orca)

    submit_parser = subparsers.add_parser(
        "submit",
        help="Prepares and submits Slurm job",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    for arg in submit_arguments:
        submit_parser.add_argument(*arg[0], **arg[1])
    submit_parser.set_defaults(func=submit)

    getfreq_parser = subparsers.add_parser(
        "getfreq", help="Returns therm. chem. results of FREQ run"
    )
    for arg in getfreq_arguments:
        getfreq_parser.add_argument(*arg[0], **arg[1])
    getfreq_parser.set_defaults(func=getfreq)

    getenergy_parser = subparsers.add_parser(
        "getenergy", help="Returns final single point energy"
    )
    for arg in getenergy_arguments:
        getenergy_parser.add_argument(*arg[0], **arg[1])
    getenergy_parser.set_defaults(func=getenergy)

    version_parser = subparsers.add_parser(
        "version", help="Displays version of molr"
    )
    version_parser.set_defaults(func=getversion)

    args = parser.parse_args()
    if args.subcommand is None:
        parser.print_help()
    else:
        if "debug" in args.__dict__.keys():
            if args.debug:
                logging.getLogger().setLevel(logging.DEBUG)
        elif "quiet" in args.__dict__.keys():
            if args.quiet:
                logging.getLogger().setLevel(logging.ERROR)
        if args.subcommand == "version":
            args.func()
        else:
            args.func(args)


if __name__ == "__main__":
    main()
