# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Creates input file for FREQ run"""
import sys
import os
import logging
import shutil
from .helpers import file_path, check_converged, backup_existing_file, argument

freqinput_arguments = [
    argument(
        "input_file",
        type=file_path,
        help="Input file of a completed geometry optimization",
    ),
    argument("-T", "--temperature", type=float, help="Temperature in Kelvin"),
    argument("-P", "--pressure", type=int, help="Pressure in atm"),
]


def check_maxcore(freq_input, config):
    maxcore = False

    for x in freq_input:
        if "%maxcore" in x.lower():
            maxcore_line = x
            maxcore = True
            break

    if not maxcore:
        logging.info(
            "Did not find '%maxcore' directive. Will add according to configuration."
        )
        mem_line = (
            "%maxcore " + str(int(0.8 * int(config["slurm"]["memory"]))) + "\n"
        )
        freq_input.append(mem_line)
        logging.info("Added: %s", mem_line.strip())
    else:
        logging.info("Found '%%maxcore' directive: %s", maxcore_line)
        logging.info("Make sure to request enough memory on submission!")

    return freq_input


def exchange_keyword(keyword_line: list, old_keyword: str, new_keyword: str):
    # First check if old_keyword is even in the keyword_line
    if not old_keyword.lower() in keyword_line.lower():
        logging.info("Did not find %s in keywords.", old_keyword.lower())
        logging.info("Will add %s anyway.", new_keyword)
        keywords = keyword_line.split()
        keywords.append(new_keyword)
        keywords.append("\n")
        keywords_join = " ".join(keywords)
        return keywords_join

    keywords = keyword_line.split()
    for idx, key in enumerate(keywords):
        if old_keyword in key.lower():
            keywords[idx] = new_keyword

    keywords.append("\n")

    keywords_join = " ".join(keywords)

    return keywords_join


def check_solvation(lines):
    freq_choice = {"CPCM": "FREQ", "SMD": "NumFREQ"}
    solvation = False
    solvation_model = None
    for line in lines:
        if "cpcm" in line.lower():
            solvation = True
            solvation_model = "CPCM"
        if "smd" in line.lower():
            solvation_model = "SMD"

        if "* xyz" in line.lower():
            break

    freq_type = freq_choice.get(solvation_model)

    return (solvation, solvation_model, freq_type)


def create_freqinput_orca(args, config):
    output_file = args.input_file[:-4] + ".freq.inp"
    backup_existing_file(output_file)
    shutil.copyfile(args.input_file, output_file)

    with open(output_file, encoding="utf-8", mode="r") as f:
        lines = f.readlines()

    logging.info("Found keywords: %s", lines[0].strip())

    lines[0] = exchange_keyword(lines[0], "opt", "FREQ")

    logging.info("Written new keywords as: %s", lines[0].strip())
    # Assuming that an .inp file is provided.
    # TODO: First check if .inp file or .xyz file is given.
    # Then chooose strategy accordingly.

    # file_ending = args.input_file[args.input_file.find('.'):]

    # construct_input = {'.xyz': construct_input_xyz,
    #                    '.inp': construct_input_inp}

    # construct_input[file_ending]()

    if check_converged(args.input_file):
        # A converged geometry optimization has been found
        # Find optimized .xyz and copy coordinates this will
        # fail if the optimization is still running!
        coords_file = args.input_file[:-4] + ".xyz"
        if not os.path.isfile(coords_file):
            logging.critical("No optimized coordinates found. Aborting...")
            sys.exit(1)

        with open(coords_file, encoding="utf-8", mode="r") as f:
            coord_lines = f.readlines()

        coords = coord_lines[2:]

        xyz_str = next(x for x in lines if "*" in x)
        xyz_idx = lines.index(xyz_str) + 1

        freq_input = []

        for x in lines[:xyz_idx]:
            freq_input.append(x)

        freq_input = check_maxcore(freq_input, config)

        for x in coords:
            freq_input.append(x.lstrip())

        freq_input.append("\n")
        freq_input.append("*")
    else:
        # No converged geometry optimization has been found
        logging.info(
            "The frequency calculation will be using the optimized xyz file."
        )
        xyz_file = args.input_file[:-4] + ".xyz"
        xyz_str = next(x for x in lines if "*" in x)
        xyz_idx = lines.index(xyz_str) + 1

        freq_input = []

        for x in lines[: xyz_idx - 1]:
            freq_input.append(x)

        freq_input = check_maxcore(freq_input, config)

        xyz_l = xyz_str.split()
        xyz_l[1] = "xyzfile"
        xyz_l.append(xyz_file)
        xyz_l.append("\n")
        xyz_str = " ".join(xyz_l)
        freq_input.append(xyz_str)

    with open(output_file, encoding="utf-8", mode="w") as f:
        f.writelines(freq_input)

    logging.info("Written input file to %s", output_file)
