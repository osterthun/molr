# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Provides function to retrieve electronic energy from completed ORCA run"""

import re
import os
import sys
import logging
from dataclasses import dataclass
from .helpers import argument


@dataclass
class EnergyResults:
    electronic_energy: float
    functional: str
    full_path: str


getenergy_arguments = [
    argument(
        "output_file", nargs="?", help="Output file to extract energy from"
    )
]

FUNCTIONALS = [
    "HFS",
    "LDA",
    "LSD",
    "VWN",
    "VWN5",
    "VWN3",
    "PWLDA",
    "BP86",
    "BP",
    "BLYP",
    "OLYP",
    "GLYP",
    "XLYP",
    "PW91",
    "mPWPW",
    "mPWLYP",
    "PBE",
    "RPBE",
    "REVPBE",
    "RPW86PBE",
    "PWP",
    "B1LYP",
    "B3LYP",
    "B3LYP/G",
    "O3LYP",
    "X3LYP",
    "B1P",
    "B3P",
    "B3PW",
    "PW1PW",
    "mPW1PW",
    "mPW1LYP",
    "PBE0",
    "REVPBE0",
    "REVPBE38",
    "BHANDHLYP",
    "TPSS",
    "TPSSh",
    "TPSS0",
    "M06L",
    "M06",
    "M062X",
    "PW6B95",
    "B97M-V",
    "B97M-D3BJ",
    "B97M-D4",
    "SCANfunc",
    "wB97",
    "wB97X",
    "wB97X-D3",
    "wB97X-D4",
    "wB97X-V",
    "wB97X-D3BJ",
    "wB97M-V",
    "wB97M-D3BJ",
    "wB97M-D4",
    "CAM-B3LYP",
    "LC-BLYP",
    "LC-PBE",
    "R2SCAN-3C",
]


def get_functional(keywords: list):
    for k in keywords:
        if k in FUNCTIONALS:
            functional = k
            return functional


def find_keywords(file):
    with open(file, encoding="utf-8", mode="r") as f:
        lines = f.read()
    input_file_section = re.search(
        r"(?<=INPUT FILE).*?(?=\*+END OF INPUT)", lines, re.DOTALL
    )
    if input_file_section:
        keywords_match = re.findall(
            r"^\s*\|\s*\d+\s*>\s*!(.+)",
            input_file_section.group(0),
            re.MULTILINE,
        )
        keywords = [
            keyword.strip()
            for line in keywords_match
            for keyword in line.split()
        ]
        return keywords
    else:
        print("ERROR: No Input Section found")


def get_energy(output_file):
    with open(output_file, encoding="utf-8", mode="r") as file:
        lines = file.readlines()

    normal_termination = False
    for line in reversed(lines):
        if "ORCA TERMINATED NORMALLY" in line:
            normal_termination = True
    if normal_termination:
        for line in reversed(lines):
            if "FINAL SINGLE POINT ENERGY" in line:
                total_energy = float(line.split()[-1])
                break
    else:
        logging.error("ORCA did not terminate normally!")
        sys.exit(1)

    keywords = find_keywords(output_file)
    functional = get_functional(keywords)
    full_path = os.path.abspath(output_file)
    result = EnergyResults(
        electronic_energy=total_energy,
        functional=functional,
        full_path=full_path,
    )
    return result
