# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Provides function to retrieve results of FREQ calculations"""

import re
import logging
import os
from dataclasses import dataclass
from .helpers import argument


@dataclass
class FreqResults:
    gibbs_correction: float
    temperature: float
    pressure: float
    full_path: str
    imaginary_mode: bool


getfreq_arguments = [
    argument("output_file", help="File of a completed frequency calculation")
]


def get_freq(output_file):
    """
    Provides Gibbs correction, temperature, pressure, and full path of FREQ run.

    Parameters
    ----------
    output_file : str
        The path to the output file containing the results of the FREQ run.

    Returns
    -------
    FreqResults
        An instance of the FreqResults data class containing the following information:
        - gibbs_correction : float
            Gibbs correction value extracted from the output file.
        - temperature : float
            Temperature value extracted from the output file.
        - pressure : float
            Pressure value extracted from the output file.
        - full_path : str
            Absolute path of the provided output file.
        - imaginary_mode : bool
            Flag indicating whether an imaginary mode was found during the run.

    Raises
    ------
    IOError
        If there is an issue reading the specified output file.

    Notes
    -----
    This function parses the specified FREQ run output file to extract relevant
    information such as Gibbs correction, temperature, pressure, full path, and
    the presence of imaginary modes.

    Example
    -------
    >>> output_file = 'path/to/freq_output.log'
    >>> result = get_freq(output_file)
    >>> print(result)
    FreqResults(gibbs_correction=1.23, temperature=300.0, pressure=1.0,
        full_path='/absolute/path/to/freq_output.log', imaginary_mode=True)
    """

    with open(output_file, encoding="utf-8", mode="r") as f:
        lines = f.readlines()

    pattern_success = r"imaginary mode"

    for line in lines:
        if re.search(pattern_success, line):
            logging.warning("Found imaginary mode!")
            imaginary_mode = True
        else:
            imaginary_mode = False

    pattern_gibbs = r"G-E\(el\)"

    for line in lines:
        if re.search(pattern_gibbs, line):
            gibbs_correction = float(line.strip().split()[2])

    pattern_temp = r"Temperature"

    for line in lines:
        if re.search(pattern_temp, line):
            temperature = float(line.strip().split()[2])

    pattern_pressure = r"Pressure"

    for line in lines:
        if re.search(pattern_pressure, line):
            pressure = float(line.strip().split()[2])

    full_path = os.path.abspath(output_file)

    return FreqResults(
        gibbs_correction, temperature, pressure, full_path, imaginary_mode
    )
