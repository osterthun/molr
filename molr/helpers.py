# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Provides general functions"""
import os
import sys
import logging


def file_path(string):
    if os.path.isfile(string):
        return string
    else:
        logging.error("File '%s' not found", string)
        sys.exit(1)


def backup_existing_file(file):
    if os.path.exists(file):
        backup_number = 1
        while os.path.exists(f"{file}.{backup_number}"):
            backup_number += 1

        # Rename existing backups
        for i in range(backup_number - 1, 0, -1):
            current_backup_path = f"{file}.{i}"
            new_backup_path = f"{file}.{i+1}"
            os.rename(current_backup_path, new_backup_path)
            logging.info(
                "Existing file '%s' backed up to '%s'.",
                current_backup_path,
                new_backup_path,
            )

        backup_file_path = f"{file}.1"
        os.rename(file, backup_file_path)
        logging.info(
            "Existing file '%s' backed up to '%s'.",
            file,
            backup_file_path,
        )


def check_converged(file):
    if not os.path.exists(file[:-4] + ".out"):
        logging.warning(
            "No output file of successful geometry optimization found."
        )
        logging.warning("The frequency calculation is likely to fail.")
    else:
        with open(file[:-4] + ".out", encoding="utf-8", mode="r") as f:
            lines = f.readlines()
        for line in lines:
            if "HURRAY" in line:
                return True
        logging.warning(
            "Geometry optimization seems to not have converged (yet)."
        )
        logging.warning("The frequency calculation is likely to fail.")
        return False


def extract_coordinates(file):
    start_heading = "CARTESIAN COORDINATES (ANGSTROEM)"
    end_heading = "CARTESIAN COORDINATES (A.U.)"
    extracting = False
    coordinates = []
    current_coordinates = []
    # Read the content of the file
    with open(file, encoding="utf-8", mode="r") as file:
        lines = file.readlines()
        for line in lines:
            line = line.strip()

            if line == start_heading:
                extracting = True
                current_coordinates = []
                continue
            elif line == end_heading:
                extracting = False
                coordinates = current_coordinates.copy()
                current_coordinates = []
                continue

            if extracting:
                current_coordinates.append(line)

    l = len(coordinates)
    return coordinates[1 : l - 2]


def argument(*name_or_flags, **kwargs):
    """Convenience function to properly format arguments to pass to the
    subcommand decorator.

    """
    return (list(name_or_flags), kwargs)
