# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# pylint: skip-file

import sys
from .helpers import file_path, backup_existing_file, argument
from dataclasses import dataclass
import logging

prepare_arguments = [
    argument("coord_file", type=file_path),
    argument(
        "-f", "--functional", type=str, default="BP86", help="DFT functional"
    ),
    argument(
        "-b", "--basis-set", type=str, default="DEF2-SVP", help="Basis set"
    ),
    argument("-c", "--charge", type=int, default=0, help="Molecular charge"),
    argument("-m", "--multiplicity", type=int, default=1, help="Multiplicity"),
    argument(
        "-u", "--user", type=str, help="Custom options to add to the input file"
    ),
    argument(
        "-O",
        "--output",
        type=str,
        default="<coord>.inp",
        help="Output file name",
    ),
    argument(
        "-o",
        "--opt",
        action="count",
        default=0,
        help="Include structure optimization",
    ),
    argument(
        "-d",
        "--dispersion",
        action="store_true",
        help="Include dispersion correction (WIP, might not work for all functionals!)",
    ),
    argument("--debug", action="store_true", help="Outputs debug information"),
    argument("--quiet", action="store_true", help="Only outputs errors"),
]


@dataclass
class GeneralInput:
    functional: str
    basis_set: str
    charge: int
    multiplicity: int
    user: str
    output: str
    opt: int
    dispersion: bool
    coord_file: str

    def __post_init__(self):
        if self.user == None:
            self.user = ""

    def orca_input(self, number_of_cpus):
        # Build the ORCA input file content based on the provided options
        with open(self.coord_file) as f:
            lines = f.readlines()[2:]
            f.close()
        coords = "".join(lines)
        dispersion_option = f"D3" if self.dispersion else ""

        opt_levels = {0: "", 1: "OPT", 2: "TightOPT", 3: "VerytightOPT"}

        opt_option = opt_levels.get(self.opt)

        input_content = f"""! RKS {self.functional} {self.basis_set} {self.user} {dispersion_option} {opt_option} 
%PAL NPROCS {number_of_cpus} END 
* xyz {self.charge} {self.multiplicity}
{coords}
*
"""

        return input_content


def prepare_orca_input(args, config):
    """Generate ORCA input files."""

    output_file = args.coord_file[:-4] + ".inp"
    # Check if the output file exists, and create backups if needed
    backup_existing_file(output_file)

    prepare = GeneralInput(
        functional=args.functional,
        basis_set=args.basis_set,
        opt=args.opt,
        user=args.user,
        charge=args.charge,
        coord_file=args.coord_file,
        dispersion=args.dispersion,
        multiplicity=args.multiplicity,
        output=output_file,
    )
    # Write the input content to the output file
    with open(output_file, "w") as file:
        file.write(prepare.orca_input(number_of_cpus=config["slurm"]["cpus"]))

    logging.info(
        f"ORCA input file '{output_file}' has been created successfully."
    )
