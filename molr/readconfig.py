# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Provides configuration handling"""
import sys
import os
import logging
import configparser


# Reads in a config.ini file with the following configuration values:
# project
# version
# versiondate
# modules
# orca_installpath (for local installs)
# openmpi_installpath (for local installs)
# default walltime
# default #cpus
# default memory
# email for notifications
# Example file:
# [molr]
# version=
# version_date=
# [modules]
# module1/3.4
# module2/5.2
# [local]
# orca_installpath=
# openmpi_installpath=
# [slurm]
# walltime=
# cpus=
# memory=
# project=
# email=


def read_config():
    config = configparser.ConfigParser()

    home = os.getenv("HOME")
    ending = ".molrrc"
    script_dir = os.path.dirname(os.path.abspath(__file__))
    repo_config_path = os.path.join(
        os.path.join(script_dir, os.pardir), ".molrrc"
    )

    if os.path.isfile(os.path.join(home, ending)):
        config_path = os.path.join(home, ending)
    elif os.path.isfile(repo_config_path):
        config_path = repo_config_path
    else:
        logging.error("No configuration file found.")
        sys.exit(1)

    config.read(config_path)
    # sections = config.sections()
    # print(sections)
    # for section in sections:
    #    print(section+":")
    #    for key in config[section]:
    #        print(key)
    #        print(config[section][key])

    return config
