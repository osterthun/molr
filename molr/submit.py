# molr - a cli tool to generate input files and retrieve results from QC calculations
# Copyright (C) 2023 Ole Osterthun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Creates and submits Slurm job"""
import os
import subprocess
import logging
from importlib import metadata
from jinja2 import Template
from .helpers import file_path, backup_existing_file, argument
from .readconfig import read_config

config = read_config()

submit_arguments = [
    argument("input_file", type=file_path, help="ORCA input file to be run"),
    argument(
        "-c",
        "--cpus",
        type=int,
        default=config["slurm"]["cpus"],
        help="Number of CPUs per node for Slurm job",
    ),
    argument(
        "-m",
        "--mem-per-cpu",
        type=int,
        default=config["slurm"]["memory"],
        help="Amount of memory allocated per CPU",
    ),
    argument(
        "-w",
        "--walltime",
        type=str,
        default=config["slurm"]["walltime"],
        help="Walltime of batch job",
    ),
    argument(
        "-j",
        "--job-name",
        type=str,
        default="orca_job",
        help="Name of the Slurm job",
    ),
    argument(
        "-d", "--depends-on", default="", help="Job id of depending calculation"
    ),
    argument(
        "-a",
        "--account",
        type=str,
        default=config["slurm"]["project"],
        help="HPC project to bill core-h to",
    ),
    argument(
        "--dry-run",
        action="store_true",
        help="Generate Slurm script but do not submit to the queue",
    ),
]


def create_submit_file(
    input_file, job_name, cpus, mem_per_cpu, walltime, account, dependency
):
    # Get the absolute path of the script
    script_dir = os.path.dirname(os.path.abspath(__file__))

    template_file_path = os.path.join(
        script_dir, "templates/slurm_script_template.j2"
    )

    with open(template_file_path, encoding="utf-8", mode="r") as template_file:
        template_content = template_file.read()
    template = Template(template_content)

    modulepath = os.getenv("MODULEPATH")

    input_path = os.path.dirname(os.path.abspath(input_file))

    # Check if # of cpus is identical between .inp file and args
    with open(
        os.path.join(input_path, input_file), encoding="utf-8", mode="r"
    ) as f:
        lines = f.readlines()
    if int(lines[1].split()[2]) != cpus:
        logging.warning(
            "Number of CPUs in input file is: %s", lines[1].split()[2]
        )
        logging.warning("You requested %s CPUs", cpus)
        logging.warning("Substituting number of CPUs in input file")
        pal_split = lines[1].split()
        pal_split[2] = str(cpus)
        pal_split.append("\n")
        pal_join = " ".join(pal_split)
        lines[1] = pal_join

        backup_existing_file(input_file)
        with open(
            os.path.join(input_path, input_file),
            encoding="utf-8",
            mode="w",
        ) as f:
            f.writelines(lines)
        logging.info("Written input file with requested number of CPUs")

    output_file = input_file[:-4] + ".out"
    backup_existing_file(output_file)

    if "hpcwork" in input_path:
        constraint = "hpcwork"
    else:
        constraint = ""

    version = metadata.version("molr")

    # Render the template with the provided options
    slurm_script_content = template.render(
        job_name=job_name,
        cpus=cpus,
        mem_per_cpu=mem_per_cpu,
        walltime=walltime,
        account=account,
        constraint=constraint,
        dependency=dependency,
        modulepath=modulepath,
        input_file=input_file,
        input_path=input_path,
        output_file=output_file,
        version=version,
    )
    return slurm_script_content


def write_submit_file(slurm_script_content, slurm_script_path):
    # Write the Slurm script to a file
    backup_existing_file(slurm_script_path)
    with open(slurm_script_path, encoding="utf-8", mode="w") as script_file:
        script_file.write(slurm_script_content)
    pass


def submit_submit_file(slurm_file, job_name):
    # Submit the job to Slurm
    subprocess.run(["sbatch", slurm_file], check=False)
    logging.info("Slurm job submitted successfully! Job name: %s", job_name)
